#include <iostream>

void pickUpEgg(bool* isThereAnEgg, int* numOfEggsOnBasket);
void addSeeds(int* numsOfSeeds);
bool canProduceAnEgg(int numTurn, bool* isThereAnEgg, int i);
void displayFarmInfo(bool* isChickenAlive, bool* isThereAnEgg, int* chickenNumOfSeeds, int i);
void populateData();
bool isValidChickenCoop(int numOfCoop);
void tick(int numOfChickens, bool* isChickenAlive, bool* isThereAnEgg, int* chickenNumOfSeeds, int numOfTurn, int numOfEggsOnBasket);

#define NOTHING 1

int main(int argc, char** argv) 
{ 
	std::cout << "Welcome to Liz's Chicken Farm\n";	
	int numOfChickens = 3; 
	bool isChickenAlive[numOfChickens] = {true, true, true}; 
	bool isThereAnEgg[numOfChickens] = {false, false, false};
	int chickenNumOfSeeds[numOfChickens] = {5, 5, 5};
	int numOfEggsOnBasket = 0;



	int userAction = 0; 

	for(int numOfTurn = 0; userAction != 4; ++numOfTurn) 
	{
		
		
		tick(numOfChickens, isChickenAlive, isThereAnEgg, chickenNumOfSeeds, numOfTurn, numOfEggsOnBasket);
		display(...);

		//Farm Activities that user can perform 
		std::cout << "What do you like to do?\n";
		std::cout << NOTHING << ": Nothing\n"; 
		std::cout << "2: Add Seeds\n"; 
		std::cout << "3: Grab Egg\n";
		std::cout << "4: Exit\n"; 
		std::cin >> userAction; 
		
		switch (userAction)
		{
			case NOTHING: 
				std::cout << "***Do nothing***\n\n";
				break;
			case 2:
			{
				addSeeds(chickenNumOfSeeds);
				break; 
			}			
			case 3:
			{	
				pickUpEgg(isThereAnEgg, &numOfEggsOnBasket);
				break; 
			}
			case 4:
				std::cout << "Leaving the farm ... Bye\n";
				break;
	
			default:
				std::cout << "Invalid activity select one of the options:\n";
				break;
		}


	}
	
	return 0; 

	
}

void pickUpEgg(bool* isThereAnEgg, int* numOfEggsOnBasket)
{
	int numOfCoop = 0;	
	std::cout << "Enter the number of Chicken coop that you like to pick an egg\n";	
	std::cin >> numOfCoop; 
	bool validCoop = isValidChickenCoop(numOfCoop);
	
	while(!validCoop)
	{
		std::cout << "Enter a valid Chicken Coop number that you like to pick an egg\n";	
		std::cin >> numOfCoop; 
		validCoop = isValidChickenCoop(numOfCoop);
	}
	

	bool egg = isThereAnEgg[numOfCoop - 1];
	if(egg)
	{
		isThereAnEgg[numOfCoop - 1] = false; 
		(*numOfEggsOnBasket)++;


		std::cout << "***You grabbed an egg. You currently have " << *numOfEggsOnBasket << " eggs in your basket****\n\n";
					
	} 
	else
	{
		std::cout << "***You can't pick up an egg you have zero eggs in this chicken coop***\n\n"; 
	}	

}

void displayFarmInfo(bool* isChickenAlive, bool* isThereAnEgg, int* chickenNumOfSeeds, int i)

{
	std::cout << "****chicken coop #" << i + 1 <<  " info****\n";

	//Display if chicken is alive
	if(isChickenAlive[i])
	{
		std::cout << "Your chicken  is alive\n";
	} 
	else 
	{
	std::cout << "Your chicken is dead\n";	
	}
	//check if there is an to pick up 
	if(isThereAnEgg[i])
	{
	std::cout << "You have 1 egg to pick up\n";
	} 
	else 
	{
		std::cout << "You have no eggs to pick up\n";
	}
		
	std::cout << "You have " << chickenNumOfSeeds[i]<< " seeds in your food storage for this chicken\n\n";

}

bool isValidChickenCoop(int numOfCoop)
{
	return numOfCoop < 4;

}

void addSeeds(int* numOfSeeds)
{
	int numOfCoop = 0; 
	bool validCoop = false;
	while(!validCoop)
	{
		std::cout << "Enter the number of chicken coop that you  like to add seeds [1..4]\n";
		std::cin >> numOfCoop;
		validCoop = isValidChickenCoop(numOfCoop);
		if (!validCoop)
		{
			std::cout << "Invalid input!\n";
		}
	}
	
	numOfSeeds[numOfCoop -1] += 5; 
	std::cout << "You add seeds to your food storage. You have " <<	numOfSeeds[numOfCoop -1] << " seeds on Chicken Coop#" << numOfCoop << "\n\n";
}

bool canProduceAnEgg(int numOfTurn, bool* isThereAnEgg, int i)
{
	return (numOfTurn != 0) && (numOfTurn % 3 == 0) && (!isThereAnEgg[i]);
}


void tick(int numOfChickens, bool* isChickenAlive, bool* isThereAnEgg, int* chickenNumOfSeeds, int numOfTurn, int numOfEggsOnBasket)
{
	std::cout << "***You have 3 chicken coops in the farm***\n\n";

	for(int i = 0; i < numOfChickens; i++)
	{
		if (isChickenAlive[i]) 
		{				
			//create an egg
			bool canAddAnEgg = canProduceAnEgg(numOfTurn, isThereAnEgg, i);
			if(canAddAnEgg )
			{
				isThereAnEgg[i] = true;
			}

			if (chickenNumOfSeeds[i] > 0)
			{
				chickenNumOfSeeds[i]--;
			}
			else
			{
				isChickenAlive[i] = false;
			}

		}

			// displayFarmInfo(isChickenAlive, isThereAnEgg, chickenNumOfSeeds, i);		
	}

	// std::cout << "You have " << numOfEggsOnBasket << " eggs in your basket\n\n";
}

void display(...) 
{
	for(...)
	{
		displayFarmInfo(...)
	}

	std::cout << numOfEggs;
}

