#include <iostream>

int main(int argc, char** argv) 
{
	std::cout << "Welcome to Liz's Chicken Farm\n";	
	bool isChickenAlive = true; 
	int numOfChickenEggs = 0; 
	int numOfEggsOnBasket = 0;
	 
	int numOfSeeds = 5; 
	int userAction = 0; 

	for(int numOfTurn = 0; userAction != 4; ++numOfTurn) 
	{

		if(numOfSeeds == 0)
		{
			isChickenAlive = false;
		}
		else
		{
			numOfSeeds--;
		}
		
		//create an egg
		bool produceAnEgg = (numOfTurn % 2 == 1) && (isChickenAlive) && (numOfChickenEggs == 0);
		if(produceAnEgg) 
		{
			numOfChickenEggs++;
		}

		//Display if chicken is alive
		if(isChickenAlive)
		{
			std::cout << "You have one chicken Alive\n\n";
		} 
		else 
		{
			std::cout << "Your chicken is dead\n\n";	
		}
		
		//Farm information 
		std::cout << "You have " << numOfChickenEggs << " egg to pick up\n";
		std::cout << "You have " << numOfSeeds << " seeds in your food storage\n";
		std::cout << "You have " << numOfEggsOnBasket << " eggs in your basket\n\n";
		
		//Farm Activities that user can perform 
		std::cout << "What do you like to do?\n";
		std::cout << "1: Nothing\n"; 
		std::cout << "2: Add Seeds\n"; 
		std::cout << "3: Grab Egg\n";
		std::cout << "4: Exit\n"; 
		std::cin >> userAction; 
		
		switch (userAction)
		{
			case 1: 
				std::cout << "Do nothing\n";
				break;
			
			case 2:
				numOfSeeds += 5; 
				std::cout << "You add seeds to your food storage. You have " << numOfSeeds << " seeds.\n\n";
				break; 
			
			case 3:
			{			
				bool canPickUpEgg = (numOfChickenEggs == 1);
				if(canPickUpEgg)
				{
					std::cout << "You grabbed an egg\n";
					numOfChickenEggs = 0; 
					numOfEggsOnBasket++;
				} 
				else
			    {
					std::cout << "You can't pick up an egg you have zero eggs\n";
				}
	
				break; 
			}
			case 4:
				std::cout << "Leaving the farm ... Bye\n";
				break;
	
			default:
				std::cout << "Invalid activity select one of the options:\n";
				break;
		}


	}
	
	return 0; 

	
}
